#Hente filen 

f = open("tall.txt", "r")

#Gjøre tekst om til tall

x = f.read().replace("femti", "50+")
x = x.replace("forti", "40+")
x = x.replace("tretti", "30+")
x = x.replace("tjue", "20+")
x = x.replace("elleve", "11+")
x = x.replace("tolv", "12+")
x = x.replace("tretten", "13+")
x = x.replace("fjorten", "14+")
x = x.replace("femten", "15+")
x = x.replace("seksten", "16+")
x = x.replace("sytten", "17+")
x = x.replace("atten", "18+")
x = x.replace("nitten", "19+")
x = x.replace("to", "2+")
x = x.replace("tre", "3+")
x = x.replace("fire", "4+")
x = x.replace("fem", "5+")
x = x.replace("seks", "6+")
x = x.replace("sju", "7+")
x = x.replace("htte", "8+")
x = x.replace("ni", "9+")
x = x.replace("ti", "10+")
x = x.replace("en", "1+")

x = x.split("+")

#Plusse tallene sammen

sum = 0

for tall in x:
    sum = int(tall) + sum
    print(sum)